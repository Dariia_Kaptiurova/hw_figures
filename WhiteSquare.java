package com.company;

public class WhiteSquare {

    void drawWhiteSquare() {

        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 5; i++) {
                if (j > 0 && j < 4 - 1 && i > 0 && i < 5-1) {
                    System.out.print("  ");
                } else System.out.print("* ");
            }
            System.out.println();
        }
    }
}
