package com.company;

public class Main {

    public static void main(String[] args) {

        BlackSquare blackSquare = new BlackSquare();
        blackSquare.drawBlackSquare();
        System.out.println();

        WhiteSquare whiteSquare = new WhiteSquare();
        whiteSquare.drawWhiteSquare();
        System.out.println();

        Circle circle = new Circle();
        circle.drawCircle();
        System.out.println();

        Rhombus rhombus = new Rhombus();
        rhombus.drawRhombus();
        System.out.println();

        Triangle1 triangle1 = new Triangle1();
        triangle1.drawTriangle1();
        System.out.println();

        Triangle2 triangle2 = new Triangle2();
        triangle2.drawTriangle2();
        System.out.println();

        Triangle3 triangle3 = new Triangle3();
        triangle3.drawTriangle3();
        System.out.println();

        Triangle4 triangle4 = new Triangle4();
        triangle4.drawTriangle4();
        System.out.println();

        Triangle5 triangle5 = new Triangle5();
        triangle5.drawTriangle5();
        System.out.println();

        Triangle6 triangle6 = new Triangle6();
        triangle6.drawTriangle6();
        System.out.println();

        Triangle7 triangle7 = new Triangle7();
        triangle7.drawTriangle7();
        System.out.println();

        Triangle8 triangle8 = new Triangle8();
        triangle8.drawTriangle8();
        System.out.println();

        CrossX crossX = new CrossX();
        crossX.drawCrossX();
        System.out.println();
    }
}
