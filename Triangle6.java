package com.company;

public class Triangle6 {

    void drawTriangle6() {

            for (int i = 4; i >= 0; i--) {
                for (int j = 0; j < 4; j++) {
                    if (j < i) {
                        System.out.print("   ");
                    } else
                        System.out.print(" * ");
                }
                System.out.println();
            }
            for (int i = 1; i <= 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (j < i) {
                        System.out.print("   ");
                    } else
                        System.out.print(" * ");
                }
                System.out.println();
            }
            System.out.println("  ");
        }
    }

