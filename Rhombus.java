package com.company;

public class Rhombus {

    void drawRhombus() {
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < 7; i++) {
                if (7 / 2 + j == i
                        || 7 / 2 - j == i - 7 + 1
                        || 7 / 2 - j == i
                        || -7 / 2 + j == i) {
                    System.out.print("* ");
                } else System.out.print("  ");
            }
            System.out.println();
        }
    }
}
