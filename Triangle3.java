package com.company;

public class Triangle3 {

    void drawTriangle3() {

        for (int i = 0; i < 6; i++) {
            for (int j = i; j < 6; j++) {
                if (i > 0 && i < 6 - 1 && j > i && j < 6 - 1) {
                    System.out.print("  ");
                } else System.out.print("+ ");
            }
            System.out.println();
        }
    }
}
