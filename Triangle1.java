package com.company;

public class Triangle1 {

    void drawTriangle1() {

        for (int i = 5; i >= 0; i--) {
            for (int j = i; j <= 5; j++) {
                if(i > 0 && i < 6 - 1 && j > i && j < 6-1) {
                    System.out.print("  ");
                }
                else System.out.print("+ ");
            }
            System.out.println();
        }
    }
}

