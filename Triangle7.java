package com.company;

public class Triangle7 {

    void drawTriangle7() {

        for (int i = 0; i <= 4; i++) {

            //Левое пустое пространство
            for (int j = 4; j > i; j--) {
                System.out.print("  ");
            }

            //Левая часть треугольника
            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 > 0 && j1 <= i && i < 4) {
                    System.out.print("  ");
                } else System.out.print("+ ");
            }

            //Правая часть треугольника
            for (int j = 0; j < i; j++) {
                if (j >= 0 && j < i - 1 && i < 4) {
                    System.out.print("  ");
                } else System.out.print("+ ");
            }

            //Правое пустое пространство
            for (int j = 4; j > i; j--) {
                System.out.print("  ");
            }
            System.out.println();
        }
    }
}
